import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {routes} from './app.router';
import {
    MatButtonModule,
    MatTabsModule,
    MatToolbarModule,
    MatIconModule,
    MatListModule,
    MatInputModule,
    MatSelectModule
} from '@angular/material';
import {AppComponent} from './app.component';
import {AppService} from './app.service';
import {LocationComponent} from './locathion/location.component';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {CategoryComponent} from './category/category.component';
import {EditCategoryComponent} from './edit-category/edit-category.component';
import {CategoryService} from './category.service';
import { LocationEditComponent } from './location-edit/location-edit.component';
import {LocationService} from './location.service';
import {LocationFilterPipe} from './location-filter.pipe';
import { Ng2MapModule} from 'ng2-map';

@NgModule({
    declarations: [
        AppComponent,
        LocationComponent,
        CategoryComponent,
        EditCategoryComponent,
        LocationEditComponent,
        LocationFilterPipe
    ],
    imports: [
        BrowserModule,
        MatButtonModule,
        MatTabsModule,
        NoopAnimationsModule,
        MatToolbarModule,
        MatIconModule,
        MatListModule,
        FormsModule,
        routes,
        MatInputModule,
        MatSelectModule,
        Ng2MapModule.forRoot({apiUrl: 'https://maps.google.com/maps/api/js?key=AIzaSyCOJYisOzcp-IuQWUh3pR8C9DfrnJB9XtA'})
    ],
    providers: [CategoryService, LocationService, AppService],
    bootstrap: [AppComponent]
})
export class AppModule {
}
