import {Injectable} from '@angular/core';
import {Category} from './category';

@Injectable()
export class CategoryService {
    categories: Category[] = JSON.parse(localStorage.getItem('categories')) ? JSON.parse(localStorage.getItem('categories')) : [];
    currentCategory: any;

    constructor() {
    }

    getCategory(): Category[] {
        return this.categories;
    }

    addCategory(id: number, title: string) {
        this.categories.push(new Category(id, title));
        localStorage.setItem('categories', JSON.stringify( this.categories));
        console.log(this.categories);
    }

    editCategory(category: Category) {
        const index = this.categories.indexOf(category);
        if (index > -1) {
            this.categories.splice(index, 1);
        }
        this.categories.push(category);
        localStorage.setItem('categories', JSON.stringify(this.categories));
    }

    deleteCategory(category: Category) {
        const index = this.categories.indexOf(category);
        if (index > -1) {
            this.categories.splice(index, 1);
            localStorage.setItem('categories', JSON.stringify( this.categories));
        }
    }

    setCurrentCategory(category: Category) {
        this.currentCategory = category;
    }

    getCurrentCategory() {
        return this.currentCategory;
    }
}
