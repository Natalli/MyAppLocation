import {Component, OnInit} from '@angular/core';
import {CategoryService} from '../category.service';
import {Category} from '../category';
import {AppComponent} from '../app.component';
import {AppService} from '../app.service';

@Component({
    selector: 'app-category',
    templateUrl: './category.component.html',
    styleUrls: ['./category.component.css']
})

export class CategoryComponent extends AppComponent implements OnInit {
    categories: Category[];

    constructor(private categoryService: CategoryService,
                public appService: AppService) {
        super(appService);
        this.categories = [];
    }

    ngOnInit() {
        this.categories = this.categoryService.getCategory();
        this.showForm = this.appService.getForm();
        this.categories.sort( function(category1, category2) {
            if ( category1.title < category2.title ){
                return -1;
            }else if( category1.title > category2.title ){
                return 1;
            }else{
                return 0;
            }
        });
    }

    edit(category: Category) {
        this.changeForm();
        this.categoryService.setCurrentCategory(category);
    }

    delete(category: Category) {
        this.categoryService.deleteCategory(category);
    }

}
