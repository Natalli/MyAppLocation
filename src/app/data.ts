export const categories = [
    {
        id: 1,
        title: 'Hospital',
        check: false
    },
    {
        id: 2,
        title: 'School',
        check: false
    }
];

export const locations = [
    {
        id: 1,
        title: 'Hospital 1',
        address: 'Bunina 13',
        coordinates: '50.465001, 30.621138',
        check: false
    },
    {
        id: 1,
        title: 'Hospital 2',
        address: 'Bunina 15',
        coordinates: '50.470190, 30.609551',
        check: false
    },
    {
        id: 2,
        title: 'School 1',
        address: 'Bunina 17',
        coordinates: '50.470190, 30.609551',
        check: false
    },
    {
        id: 2,
        title: 'School 2',
        address: 'Bunina 19',
        coordinates: '50.470190, 30.609551',
        check: false
    }
];