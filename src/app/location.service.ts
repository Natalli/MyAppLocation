import {Injectable} from '@angular/core';
import {Location} from './location';



@Injectable()
export class LocationService {
  locations: Location[] = JSON.parse(localStorage.getItem('locations')) ? JSON.parse(localStorage.getItem('locations')) : [];
  currentLocation: any;
  constructor() {
  }

  getLocation(): Location[] {
    return this.locations;
  }

  addLocation(currentLocation) {
    const location = new Location(currentLocation.id, currentLocation.title, currentLocation.address, currentLocation.category_id, currentLocation.coordinates);
    this.locations.push(location);
    localStorage.setItem('locations', JSON.stringify( this.locations));
  }

  editLocation(location: Location) {
    const index = this.locations.indexOf(location);
    if (index > -1) {
      this.locations.splice(index, 1);
    }
    this.locations.push(location);
    localStorage.setItem('locations', JSON.stringify(this.locations));
  }

  deleteLocation(location: Location) {
    const index = this.locations.indexOf(location);
    if (index > -1) {
      this.locations.splice(index, 1);
    }
      localStorage.setItem('locations', JSON.stringify( this.locations));
  }

  setCurrentLocation(location: Location) {
    this.currentLocation = location;
  }

  getCurrentLocation() {
    return this.currentLocation;
  }
}
